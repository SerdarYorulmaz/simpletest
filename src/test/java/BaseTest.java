import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.Test;

public class BaseTest {

    WebDriver driver = new ChromeDriver();

    @Test
    public void menuControl() {
        driver.get("https://www.haberturk.com/");
        //System.out.println(driver.getTitle());
        WebElement elem = driver.findElement(By.linkText("Spor"));
        System.out.println(elem.getText());
        elem.click();
    }

    @Test
    public void titleControl() {
        driver.get("https://www.haberturk.com/");
        //System.out.println(driver.getTitle());
        // WebElement elem=driver.findElement(By.linkText("Spor"));
        WebElement elem = driver.findElement(By.xpath("//div[@title='HaberTuneliDesktop']"));
        System.out.println(elem.getText());
        elem.click();
    }

    @Test
    public void title2Control() {
        driver.get("https://www.google.com/");
        WebElement elem = driver.findElement(By.name("q"));
        elem.sendKeys("deneme1");
        driver.quit();

    }

    @Test
    public void getList() throws InterruptedException {
        driver.get("https://www.foreks.com/");
        driver.findElement(By.className("close")).click();
        Thread.sleep(300);
        WebElement element = driver.findElement(By.xpath("//*[@class='menu']//*[@class='menuIn']"));
        System.out.println(element.getText());


    }
}
