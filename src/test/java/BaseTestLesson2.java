import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.Test;

import java.util.List;

public class BaseTestLesson2 {

    WebDriver driver = new ChromeDriver();

    @Test
    public void getList() {
        driver.get("https://www.foreks.com/");
        // driver.findElement(By.linkText("Haberler")).click();
        WebElement element = driver.findElement(By.className("menuIn"));
        List<WebElement> headerMenuBtn = element.findElements(By.tagName("li"));

        /* forech daha kolay asagidaki uzun yol
        for (int i = 0; i < headerMenuBtn.size(); i++) {
            WebElement menuText = headerMenuBtn.get(i);
            if (menuText.getText().equals("Borsa")) {
                menuText.click();
                break;
            }
        }*/

        for(WebElement lst:headerMenuBtn){
            if(lst.getText().equals("Borsa")){
                lst.click();
                break;
            }
        }
        driver.quit();

    }
}
